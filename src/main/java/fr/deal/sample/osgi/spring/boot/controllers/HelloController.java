package fr.deal.sample.osgi.spring.boot.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class HelloController {

    public HelloController(){
        System.out.println("HelloController");
    }

    @GetMapping
    public String index() {
        return "Greetings from Spring Boot on OSGI!";
    }

}
