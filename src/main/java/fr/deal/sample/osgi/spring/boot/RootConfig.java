package fr.deal.sample.osgi.spring.boot;

import fr.deal.sample.osgi.spring.boot.controllers.ControllerConfig;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan("fr.deal")
@Import(ControllerConfig.class)
public class RootConfig {
}
