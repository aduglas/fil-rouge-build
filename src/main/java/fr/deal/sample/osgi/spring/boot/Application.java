package fr.deal.sample.osgi.spring.boot;

import org.osgi.framework.BundleContext;

import java.util.Map;

@org.osgi.service.component.annotations.Component
public class Application extends ActivatorBase4SpringBoot {

	public Application(){
		System.out.println("Application");
	}

	@Override
	protected void registerServices() {

	}

	@Override
	protected void modified(Map<String, Object> properties, BundleContext ctx) {
		System.out.println("modified");
	}

	@Override
	protected void beforeStarting(BundleContext context) {
		System.out.println("Before starting");
	}

	@Override
	protected void afterStart(BundleContext context) {
		System.out.println("After starting");
	}

	@Override
	protected void beforeStopping(BundleContext context) {
		System.out.println("Before stopping");
	}

	@Override
	protected void afterStop(BundleContext context) {
		System.out.println("After stopping");
	}
}
