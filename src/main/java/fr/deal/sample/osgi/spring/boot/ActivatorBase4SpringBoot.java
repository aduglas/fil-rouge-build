package fr.deal.sample.osgi.spring.boot;

import org.osgi.framework.BundleContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Import;

import java.util.Map;

@SpringBootApplication
@Import(RootConfig.class)
public abstract class ActivatorBase4SpringBoot {
    public static BundleContext bundleContext;
    ConfigurableApplicationContext appContext;
    public static BundleContext getBundleContext() {
        return bundleContext;
    }

    protected abstract void registerServices();
    protected abstract void modified(Map<String, Object> properties , BundleContext ctx);
    protected abstract void beforeStarting(BundleContext context);
    protected abstract void afterStart(BundleContext context);
    protected abstract void beforeStopping(BundleContext context);
    protected abstract void afterStop(BundleContext context);

    public ActivatorBase4SpringBoot(){
        System.out.println("ActivatorBase4SpringBoot");
    }

    @Deactivate
    public void innerStop(BundleContext context) throws Exception {
        beforeStopping(context);
        bundleContext = null;
        SpringApplication.exit(appContext, () -> 0);
        afterStop(context);
    }

    @Modified
    void innerModified(Map<String, Object> properties , BundleContext ctx) {
        modified(properties,ctx);
    }

    @Activate
    public void innerStart(BundleContext context) throws Exception {
        beforeStarting(context);
        Thread.currentThread().setContextClassLoader(this.getClass().getClassLoader());
        appContext = SpringApplication.run(ActivatorBase4SpringBoot.class);
        bundleContext = context;
        registerServices();
        afterStart(context);
    }
}
